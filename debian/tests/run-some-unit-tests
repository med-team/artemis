#!/bin/bash
set -e

# autopkgtest check: run some unit tests. Some other tests try to launch X11
# or connect through ssh, having the selected one run should be good enough.
# (C) 2020 Pierre Gruet.
# Author: Pierre Gruet <pgt@debian.org>


pkg=artemis

export LC_ALL=C.UTF-8
if [ "${AUTOPKGTEST_TMP}" = "" ] ; then
  AUTOPKGTEST_TMP=$(mktemp -d /tmp/${pkg}-test.XXXXXX)
  trap "rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
fi

cp -a src/ "${AUTOPKGTEST_TMP}"

cd "${AUTOPKGTEST_TMP}"
cd src/test/java/

# Putting the way to the installed jar in the classpath.
export CLASSPATH=$CLASSPATH:.:/usr/share/java/artemis.jar:/usr/share/java/junit4.jar:/usr/share/java/mockito-core.jar

cat <<EOF > JunitTestSuite.java
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        uk.ac.sanger.artemis.util.IconManagerTest.class,
        uk.ac.sanger.artemis.circular.digest.CircularGenomeControllerTest.class,
        uk.ac.sanger.artemis.circular.digest.UtilsTest.class
})

public class JunitTestSuite {   
}  	
EOF

# Create runner class
cat <<EOF > TestRunner.java
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
   public static void main(String[] args) {
      Result result = JUnitCore.runClasses(JunitTestSuite.class);
		
      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
      
      if (result.getFailureCount() > 0)
            System.exit(1);
   }
}  	
EOF


# Building test classes.
javac `find . -name "*.java"`

# Running the tests, with 1946 MB (ie 1.9 GB) so that they run on 32-bit arches
java -Xmx1946m -noverify -Djdbc.drivers=org.postgresql.Driver -DEMBOSS_ROOT="/usr" --illegal-access=warn -Dtimeout=8000 -Duse_separate_classloader=false TestRunner
